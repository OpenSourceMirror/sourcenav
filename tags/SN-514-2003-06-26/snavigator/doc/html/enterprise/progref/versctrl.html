<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
   <meta http-equiv="Content-Style-Type" content="text/css">
   <meta name="GENERATOR" content="Mozilla/4.51 [en] (WinNT; U) [Netscape]">
   <title>Integrating with Version Control Systems</title>
</head>
<body bgcolor="#FFFFFF">

<h1 CLASS="Chapter">
<a NAME="pgfId-996750"></a>&nbsp;<a NAME="pgfId-996752"></a><a NAME="versctrl"></a>Integrating
with Version Control Systems</h1>
<img SRC="versctrl-1.gif" >

<p CLASS="Body"><a NAME="pgfId-1183766"></a>Source-Navigator provides a
graphical user interface to version control systems. Cygnus Solutions has
performed product integrations with a number of popular version control
packages such as the GNU Revision Control System (RCS), Concurrent Versions
System (CVS), the Source Code Control System (SCCS), and Rational's ClearCase.

<p CLASS="Body"><a NAME="pgfId-997972"></a>Source-Navigator's user interface
provides the user with a standard set of version control operations. The
software development kit (SDK) allows third parties to integrate version
control packages so that some or all of these operations can be performed
from within Source-Navigator. This chapter assumes a basic understanding
of the Tcl programming language and familiarity with the version control
system that is to be integrated with Source-Navigator.
<h2 CLASS="Heading1">
<a NAME="pgfId-997980"></a><a NAME="basics"></a>Basics</h2>

<div CLASS="Body"><a NAME="pgfId-997982"></a>The version control interface
in Source-Navigator assumes that there is a basic set of version control
operations in the context of source code comprehension:</div>

<ul>
<li CLASS="Bullet">
<a NAME="pgfId-997986"></a>obtaining different versions of a source file
from a repository or archive maintained by the version control system.
This operation will be referred to as a <i>check-out</i>.</li>

<li CLASS="Bullet">
<a NAME="pgfId-998587"></a>examining the differences between the current
working version of a source file and another version held under version
control. This operation will be referred to as a <i>diff</i>.</li>

<li CLASS="Bullet">
<a NAME="pgfId-998599"></a>making changes to the most current version of
a file and placing those changes under version control. This operation
will be referred to as a <i>check-in</i>.</li>

<li CLASS="Bullet">
<a NAME="pgfId-998608"></a>deleting versions from the repository.</li>

<li CLASS="Bullet">
<a NAME="pgfId-998613"></a>acquiring exclusive access to a particular version
of a file so that no other developer can make changes to that version.
This operation will be referred to as <i>locking</i>.</li>

<li CLASS="Bullet">
<a NAME="pgfId-998633"></a>relinquishing exclusive access to a particular
version of a file so that other developers may make modifications. This
operation will be referred to as <i>unlocking</i>.</li>

<li CLASS="Bullet">
<a NAME="pgfId-998642"></a>discarding any changes to the current working
version and reverting to the most recent version in the repository.</li>

<li CLASS="Bullet">
<a NAME="pgfId-998649"></a>obtaining the history of a file from the version
control system. This includes:</li>

<ul>
<li CLASS="Bullet2">
<a NAME="pgfId-998659"></a>descriptive text about the changes made for
each version.</li>

<li CLASS="Bullet2">
<a NAME="pgfId-999380"></a>a list of symbolic names for the versions associated
with a file. In GNU RCS, for example, these names are known as
<i>tags</i>.</li>
</ul>
</ul>

<h2 CLASS="Heading1">
<a NAME="pgfId-998023"></a><a NAME="options"></a>Version Control Operations</h2>

<div CLASS="Body"><a NAME="pgfId-998025"></a>Source-Navigator takes a universal
approach to interfacing with external version control systems: each operation
is performed by executing a particular command line and capturing the output
from that command.
<br>&nbsp;</div>

<div CLASS="Body"><a NAME="pgfId-998029"></a>For some operations, the output
of the command is significant and only information relevant to the user
must be extracted from the output. Examples of this include obtaining a
history of changes made to a file, obtaining the names of symbolic tags,
and obtaining all of the version numbers associated with a file. The Source-Navigator
SDK provides a mechanism based on Tcl regular expressions for extracting
the relevant text from command output.</div>

<h3 CLASS="Heading2">
<a NAME="pgfId-998037"></a><a NAME="11295"></a>The Configuration File</h3>

<div CLASS="Body"><a NAME="pgfId-998039"></a>The Source-Navigator configuration
file, <tt>.../share/etc/sn_prop.cfg</tt> , is read at start-up and can
be used to customize Source-Navigator. In particular, new version control
systems may be integrated from within this file. New systems are added
by calling a Tcl procedure called <tt>sn_add_version_control_system</tt>.
<br>&nbsp;</div>

<div CLASS="Body"><a NAME="pgfId-998045"></a>The signature of this procedure
is:</div>

<blockquote><a NAME="pgfId-998047"></a><tt>sn_add_version_control_system
{<i>ident args</i>}</tt></blockquote>

<div CLASS="Body"><a NAME="pgfId-998049"></a>where</div>

<blockquote>
<div CLASS="BodyListFollow"><a NAME="pgfId-999538"></a><i><tt>ident</tt></i>
is an internal identifier used by Source-Navigator. This should be a logical
derivation of the name of the version control system and consist only of
alphanumeric characters and start with an alphabetic character. Examples
are:</div>

<blockquote>
<pre CLASS="CodeExample"><a NAME="pgfId-999445"></a>rcs
cvs
ccase3</pre>
</blockquote>

<div CLASS="BodyListFollow"><a NAME="pgfId-998058"></a><i><tt>args</tt></i>
is a variable number of arguments and may be any of the options described
in the <a href="versctrl.html#10979" CLASS="XRef">Options</a> section,
followed by a suitable value. For example:</div>

<blockquote>
<pre CLASS="CodeExample"><a NAME="pgfId-999456"></a>-checkout co</pre>
</blockquote>
</blockquote>

<h4 CLASS="Heading3">
<a NAME="pgfId-999539"></a><a NAME="10979"></a>Options</h4>

<div CLASS="Body"><a NAME="pgfId-998063"></a>For options that have simple
values, the possible values are outlined and the default is given. If this
default value is satisfactory, the option may be omitted from the call.
<br>&nbsp;</div>

<div CLASS="Body"><a NAME="pgfId-1000218"></a>For options that specify
command lines, the usage of the command is shown. If the option is not
applicable to a particular command, omitting it from the call is permissible,
but may result in some version control operations being unavailable to
the user.
<br>&nbsp;</div>

<div CLASS="Body"><a NAME="pgfId-1000223"></a>Using the <tt>-checkout</tt>
example in <a href="versctrl.html#64636" CLASS="XRef">-checkout</a>, the
checkout command line will be illustrated in the following notation:</div>

<pre CLASS="CodeExample"><a NAME="pgfId-998076"></a> ${checkout} <i><tt>filename</tt></i></pre>

<div CLASS="Body"><a NAME="pgfId-998078"></a>At runtime, the command line
that is executed might be:</div>

<pre CLASS="CodeExample"><a NAME="pgfId-998080"></a> co main.c</pre>

<div CLASS="Body"><a NAME="pgfId-998083"></a><b><tt>args</tt></b> may be
any of the following options:</div>

<blockquote><a NAME="pgfId-998085"></a><b><tt>-checkin</tt></b>
<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-1097705"></a>This specifies
the command line to check a modified file back into the repository.</div>

<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998090"></a>Command usage:</div>

<blockquote><a NAME="pgfId-1090810"></a><tt>${checkin} <i>filename</i></tt>or:
<br><tt>${checkin} <i>comment-text filenames</i> </tt>or:
<br><tt>${checkin} <i>comment-filename filenames</i></tt></blockquote>
</blockquote>
</blockquote>
</blockquote>

<blockquote>
<blockquote>
<blockquote>
<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998094"></a>Which of these
command lines will be used depends on the value of <tt>-checkin-comment-via</tt>.</div>
</blockquote>
</blockquote>
</blockquote>
</blockquote>

<blockquote><a NAME="pgfId-1000023"></a><a NAME="pgfId-998097"></a><b><tt>-checkin-comment-via</tt></b></blockquote>

<blockquote>
<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-1097753"></a>This option specifies
how comments about changes made to a file must be passed to the <tt>-checkin</tt>
command. The three possible values are:</div>
</blockquote>
</blockquote>

<table>
<tr VALIGN=TOP>
<td>
<div CLASS="CellBody">
<blockquote>
<blockquote>
<blockquote><a NAME="pgfId-1097768"></a><tt>stdin</tt></blockquote>
</blockquote>
</blockquote>
</div>
</td>

<td>
<blockquote>
<blockquote>
<blockquote>
<div CLASS="CellBody"><a NAME="pgfId-1097770"></a>The comment is issued
on the checkin command's standard input.</div>
</blockquote>
</blockquote>
</blockquote>
</td>
</tr>

<tr VALIGN=TOP>
<td>
<blockquote>
<blockquote>
<blockquote>
<div CLASS="CellBody"><a NAME="pgfId-1097772"></a><tt>file</tt></div>
</blockquote>
</blockquote>
</blockquote>
</td>

<td>
<blockquote>
<blockquote>
<blockquote>
<div CLASS="CellBody"><a NAME="pgfId-1097774"></a>The command is placed
into a temporary file and the filename is passed on the command line prior
to the names of source files.</div>
</blockquote>
</blockquote>
</blockquote>
</td>
</tr>

<tr VALIGN=TOP>
<td>
<blockquote>
<blockquote>
<blockquote>
<div CLASS="CellBody"><a NAME="pgfId-1097776"></a><tt>cmdline</tt></div>
</blockquote>
</blockquote>
</blockquote>
</td>

<td>
<blockquote>
<blockquote>
<blockquote>
<div CLASS="CellBody"><a NAME="pgfId-1097778"></a>The comment is placed
on the command line prior to the filename(s).</div>
</blockquote>
</blockquote>
</blockquote>
</td>
</tr>
</table>

<blockquote>
<blockquote>
<blockquote>
<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998114"></a>Default:
<br>None.</div>
</blockquote>
</blockquote>
</blockquote>
</blockquote>

<blockquote>
<div CLASS="Body"><a NAME="pgfId-999593"></a><a NAME="pgfId-998116"></a><b><tt>-checkin-exclusive</tt></b></div>

<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998118"></a>This option specifies
the command line to check a file into the repository and to acquire a lock
once the check-in is complete. This is useful when you want to make successive
changes to a file.</div>

<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998123"></a>Command usage:</div>
<tt>{$checkin-exclusive}</tt> <i><tt>filenames</tt></i></blockquote>
</blockquote>
</blockquote>

<blockquote>
<div CLASS="Body"><a NAME="pgfId-999631"></a><a NAME="pgfId-999616"></a><b><tt>-checkout</tt></b><a NAME="64636"></a></div>

<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-1097846"></a>This option specifies
the command line to check out the latest version of a file from the repository.</div>

<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998130"></a>Command usage:</div>
<tt>${checkout}</tt> <i><tt>filenames</tt></i></blockquote>
</blockquote>
</blockquote>

<blockquote>
<div CLASS="Body"><a NAME="pgfId-999654"></a><a NAME="pgfId-999633"></a><b><tt>-checkout-exclusive</tt></b></div>

<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998134"></a>This option specifies
the command line to check out the latest version of a file from the repository
with exclusive access that prevents other developers from modifying the
file. Some version control systems require that a file be checked out exclusively
before a modified version of it may be checked in.</div>
</blockquote>

<blockquote>
<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998140"></a>Command usage:</div>
<tt>${checkout-exclusive}</tt> <i><tt>filenames</tt></i></blockquote>
</blockquote>

<div CLASS="Body"><a NAME="pgfId-999655"></a><a NAME="pgfId-998142"></a><b><tt>-checkout-individual</tt></b></div>

<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998144"></a>This option specifies
the command line to check out a particular version of a file from the repository.</div>

<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998147"></a>Command usage:</div>
<tt>${checkout-individual}</tt> <i><tt>version-num</tt>&nbsp; <tt>filenames</tt></i></blockquote>
</blockquote>
</blockquote>

<blockquote>
<div CLASS="Body"><a NAME="pgfId-1000017"></a><a NAME="pgfId-998149"></a><b><tt>-checkout-individual-to-stdout</tt></b></div>

<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998151"></a>This option specifies
the command line to check out a particular version of a file from the repository
and echo the contents to the standard output.</div>

<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998155"></a>Command usage:</div>
<tt>${checkout-individual-to-stdout}</tt> <i><tt>version-num filenames</tt></i></blockquote>
</blockquote>
</blockquote>

<blockquote>
<div CLASS="Body"><a NAME="pgfId-1000018"></a><a NAME="pgfId-998158"></a><b><tt>-checkout-with-lock</tt></b></div>

<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998160"></a>If this option
is set to <tt>yes</tt>, then checkout operations will by default attempt
to acquire a lock to prevent other developers from making concurrent modifications.
This option determines whether the <tt>with</tt> <tt>lock</tt> check button
is initially set in the <tt>Checkout</tt> dialog box.</div>
</blockquote>

<blockquote>
<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998166"></a>Default:
<br>None.</div>
</blockquote>
</blockquote>

<div CLASS="Body"><a NAME="pgfId-1000019"></a><a NAME="pgfId-998168"></a><b><tt>-default</tt></b></div>

<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998170"></a>If this option
is set to <tt>yes</tt>, then this version control system entry will be
the default for new projects. If this option is set to <tt>yes</tt> for
more than one entry in the configuration file, then the entry which appears
last in the file will be the default.</div>
</blockquote>

<blockquote>
<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998176"></a>Default:
<br>None.</div>
</blockquote>
</blockquote>
</blockquote>

<blockquote>
<div CLASS="Body"><a NAME="pgfId-1000020"></a><a NAME="pgfId-998178"></a><b><tt>-delete-revision</tt></b></div>

<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998180"></a>This option specifies
the command line to delete a particular version of a file from the repository.</div>

<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998183"></a>Command usage:</div>
<tt>${delete-revision}</tt> <i><tt>version-num&nbsp;</tt> <tt>filename</tt></i></blockquote>
</blockquote>
</blockquote>

<blockquote>
<div CLASS="Body"><a NAME="pgfId-1000021"></a><a NAME="pgfId-998185"></a><b><tt>-diff-command</tt></b></div>

<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998187"></a>This option specifies
the name of the command used to compare two files (or two versions of the
same file).</div>
</blockquote>

<blockquote>
<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998190"></a>Default:</div>
<tt>diff</tt></blockquote>
</blockquote>

<div CLASS="Body"><a NAME="pgfId-1000022"></a><a NAME="pgfId-1171536"></a><a NAME="pgfId-998192"></a><tt>-<b>diff-ignore-case</b></tt></div>

<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998194"></a>This option specifies
any additional command line options to the <tt>diff</tt> command to cause
it to perform case insensitive comparisons. For almost all implementations
of the traditional <tt>diff</tt> command, this will be <tt>-i</tt>.</div>
</blockquote>

<blockquote>
<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998199"></a>Default:</div>
<tt>-i</tt></blockquote>
</blockquote>

<div CLASS="Body"><a NAME="pgfId-1000024"></a><a NAME="pgfId-998201"></a><b><tt>-diff-ignore-whitespace</tt></b></div>
</blockquote>

<blockquote>
<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998203"></a>This option specifies
any additional command line options to the <tt>diff</tt> command to cause
it to perform comparisons that are insensitive to whitespace. For almost
all implementations of the traditional <tt>diff</tt> command, this will
be <tt>-w</tt>.</div>
</blockquote>
</blockquote>

<blockquote>
<blockquote>
<blockquote>
<div CLASS="Body-Indent2"><a NAME="pgfId-998208"></a>Default:</div>
<tt>-w</tt></blockquote>
</blockquote>

<div CLASS="Body"><a NAME="pgfId-1000025"></a><a NAME="pgfId-998210"></a><b><tt>-discard</tt></b></div>

<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998212"></a>This option specifies
a command line to discard any modifications made to the working version
of a file and revert it to the current version in the repository.</div>
</blockquote>

<blockquote>
<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998216"></a>Command usage:</div>
<tt>${discard}</tt> <i><tt>filenames</tt></i></blockquote>
</blockquote>

<div CLASS="Body"><a NAME="pgfId-1000026"></a><a NAME="pgfId-998218"></a><b><tt>-history</tt></b></div>
</blockquote>

<blockquote>
<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998220"></a>This option specifies
a command line to retrieve the history of a file. Typically this includes
comments made by developers at each version of the file, the set of version
numbers for the file and the time and date of each change.</div>
</blockquote>
</blockquote>

<blockquote>
<blockquote>
<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998225"></a>Command usage:</div>
<tt>${history}</tt> <i><tt>filename</tt></i></blockquote>
</blockquote>

<div CLASS="Body"><a NAME="pgfId-1000027"></a><a NAME="pgfId-998227"></a><b><tt>-history-pattern</tt></b></div>
</blockquote>

<blockquote>
<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998229"></a>This option specifies
a pattern to extract the version history from the output of <tt>${history}</tt>.
It should extract all the relevant information about all versions of a
file. See <a href="versctrl.html#18338" CLASS="XRef">Patterns</a> for more
detailed information about possible values for this option.</div>
</blockquote>
</blockquote>

<blockquote>
<blockquote>
<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998235"></a>Default:</div>
None.</blockquote>
</blockquote>
</blockquote>

<blockquote>
<div CLASS="Body"><a NAME="pgfId-1000028"></a><a NAME="pgfId-998237"></a><b><tt>-history-individual</tt></b></div>
</blockquote>

<blockquote>
<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998239"></a>This option specifies
the command to retrieve the history for a particular version of a file.</div>
</blockquote>
</blockquote>

<blockquote>
<blockquote>
<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998242"></a>Command usage:</div>
<tt>${history-individual}</tt> <i><tt>version-num</tt>&nbsp; <tt>filename</tt></i></blockquote>
</blockquote>
</blockquote>

<blockquote>
<div CLASS="Body"><a NAME="pgfId-1000029"></a><a NAME="pgfId-998243"></a><b><tt>-history-individual-pattern</tt></b></div>

<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998246"></a>This option specifies
a pattern to extract the version history from the output of <tt>${history-individual}</tt>.
It should extract all the relevant information about the particular revision
of a file.</div>
</blockquote>

<blockquote>
<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998251"></a>Default:</div>
None.</blockquote>
</blockquote>

<div CLASS="Body"><a NAME="pgfId-1000030"></a><a NAME="pgfId-998253"></a><b><tt>-history-replacements</tt></b></div>

<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998255"></a>This option specifies
a set of textual replacements to perform on text extracted by the patterns
<tt>${history-pattern}</tt>
and <tt>${history-individual-pattern}</tt>. This allows text to be manipulated
so that it may be cosmetically improved before being shown in the <b>History</b>
window.</div>

<div CLASS="BodyListFollow2"><a NAME="pgfId-998261"></a>See <a href="versctrl.html#22560" CLASS="XRef">Replacements</a>
for details about possible values for this option.</div>
</blockquote>

<blockquote>
<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998264"></a>Default:</div>
None.</blockquote>
</blockquote>

<div CLASS="Body"><a NAME="pgfId-1000031"></a><a NAME="pgfId-998266"></a><b><tt>-ignore-dirs</tt></b></div>
</blockquote>

<blockquote>
<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998268"></a>This option specifies
one or more directories to ignore when presenting directory trees to the
user. Such directories may include a repository subdirectory for each directory
maintained by the version control system. When specifying more than one
subdirectory, use the Tcl list notation. For example: <tt>{foo bar}</tt></div>
</blockquote>
</blockquote>

<blockquote>
<blockquote>
<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998275"></a>Default:</div>
None.</blockquote>
</blockquote>

<div CLASS="Body"><a NAME="pgfId-1000032"></a><a NAME="pgfId-998277"></a><b><tt>-lock</tt></b></div>
</blockquote>

<blockquote>
<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998279"></a>This option specifies
the command line to lock a file such that no other developer may make concurrent
modifications.</div>
</blockquote>
</blockquote>

<blockquote>
<blockquote>
<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998283"></a>Command usage:</div>
<tt>${lock}</tt> <tt>filename</tt></blockquote>
</blockquote>
</blockquote>

<blockquote>
<div CLASS="Body"><a NAME="pgfId-1000033"></a><a NAME="pgfId-998285"></a><b><tt>-lock-individual</tt></b></div>
</blockquote>

<blockquote>
<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998287"></a>This option specifies
the command line to lock a particular version of a file such that no other
developer may make concurrent modifications to that version of the file.</div>
</blockquote>
</blockquote>

<blockquote>
<blockquote>
<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998291"></a>Command usage:</div>
<tt>${lock-individual}</tt> <tt>version-num</tt> <tt>filename</tt></blockquote>
</blockquote>
</blockquote>

<blockquote>
<div CLASS="Body"><a NAME="pgfId-1000034"></a><a NAME="pgfId-999945"></a><b><tt>-revision-number-pattern</tt></b></div>

<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998295"></a>This option specifies
a pattern to extract all of the available revision numbers from the output
of the <tt>${history}</tt> command.</div>
</blockquote>
</blockquote>

<blockquote>
<blockquote>
<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998299"></a>Default:</div>
None.</blockquote>
</blockquote>
</blockquote>

<div CLASS="Body">
<blockquote><a NAME="pgfId-1000035"></a><a NAME="pgfId-998301"></a><b><tt>-symbolic-tags-pattern</tt></b></blockquote>
</div>

<blockquote>
<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998303"></a>This option specifies
a pattern to extract the names of a file's symbolic tags from the output
of the <tt>${history}</tt> command.</div>
</blockquote>

<blockquote>
<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998307"></a>Default:</div>

<p><br>None.</blockquote>
</blockquote>

<div CLASS="Body"><a NAME="pgfId-1000036"></a><a NAME="pgfId-998309"></a><b><tt>-symbolic-tags-replacements</tt></b></div>

<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998311"></a>This option specifies
a set of textual replacements to perform on text that is extracted by the
<tt>${symbolic-tags-pattern}</tt>
option. This allows text to be manipulated so that it may be cosmetically
improved before being shown in the <b>Symbolic tags</b> window.</div>

<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998317"></a>Default:</div>

<p><br>None.</blockquote>
</blockquote>

<div CLASS="Body"><a NAME="pgfId-1000037"></a><a NAME="pgfId-1098042"></a><b><tt>-title</tt></b></div>

<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-1098063"></a>This option specifies
a descriptive label that will be presented to the user in the project preferences
window. This option is useful for showing a mixed-case product name or
one that contains whitespace.</div>

<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998326"></a>Default:</div>

<p><br>the <tt>ident</tt> parameter (in upper-case).</blockquote>
</blockquote>

<div CLASS="Body"><a NAME="pgfId-1000038"></a><a NAME="pgfId-998328"></a><b><tt>-unlock</tt></b></div>

<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998330"></a>This option specifies
the command line needed to unlock a file so that other developers may make
concurrent modifications.</div>

<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998334"></a>Command usage:</div>

<p><br><tt>${unlock}</tt> <tt>filename</tt></blockquote>
</blockquote>
</blockquote>

<blockquote>
<div CLASS="Body"><a NAME="pgfId-1000041"></a><a NAME="pgfId-1000015"></a><b><tt>-unlock-individual</tt></b></div>

<blockquote>
<div CLASS="BodyListFollow2"><a NAME="pgfId-998338"></a>This specifies
the command line to unlock a particular version of a file so that other
developers may make concurrent modifications to that version of the file.</div>

<blockquote>
<div CLASS="BodyListFollow3"><a NAME="pgfId-998342"></a>Command usage:</div>

<p><br><tt>${unlock-individual}</tt> <tt>version-num</tt> <tt>filename</tt></blockquote>
</blockquote>
</blockquote>

<h3 CLASS="Heading2">
<a NAME="pgfId-998344"></a><a NAME="18338"></a>Patterns</h3>

<div CLASS="Body"><a NAME="pgfId-998346"></a>Patterns are used to extract
text resulting from certain version control commands. It is necessary to
use patterns to filter relevant lines of text from the output.
<br>&nbsp;</div>

<div CLASS="Body"><a NAME="pgfId-998350"></a>A pattern is specified as
either:</div>

<blockquote>
<div CLASS="BodyListFollow">
<blockquote>
<li>
<a NAME="pgfId-998352"></a>A list of regular expression pairs specifying
where to start and stop extracting text. The two keywords <tt>start</tt>
and <tt>end</tt> are reserved and refer to the start and end of the body
of text respectively, or</li>
</blockquote>
</div>

<blockquote>
<li CLASS="BodyListFollow">
<a NAME="pgfId-998357"></a>An atomic regular expression specifying lines
that will be extracted if the regular expression matches any text in the
line.</li>
</blockquote>
</blockquote>

<div CLASS="Body"><a NAME="pgfId-998361"></a>Some examples of patterns
are:</div>

<blockquote>
<div CLASS="BodyListFollow"><a NAME="pgfId-998363"></a>Extract the names
of all the directories in <tt>ls -l</tt> output:
<br>&nbsp;</div>

<div CLASS="BodyListFollow2"><a NAME="pgfId-998365"></a><tt>&nbsp; ^d</tt>
(an atomic regular expression)
<br>&nbsp;</div>

<div CLASS="BodyListFollow"><a NAME="pgfId-998367"></a>Extract the names
of all the files and directories in <tt>ls -al</tt> output, but exclude
the current and parent directories ("." and ".."):</div>


<p CLASS="BodyListFollow2"><a NAME="pgfId-998371"></a><tt>&nbsp; { {</tt>
"<tt> ..</tt> "<tt> end} } </tt>(a list of 1 regular expression pair)

<p CLASS="BodyListFollow"><a NAME="pgfId-998373"></a>More sophisticated
examples exist in the configuration example in <a href="versctrl.html#14889" CLASS="XRef">Example</a>.</blockquote>

<h3 CLASS="Heading2">
<a NAME="pgfId-998376"></a><a NAME="22560"></a>Replacements</h3>

<div CLASS="Body"><a NAME="pgfId-998378"></a>Replacements may be used to
modify or delete text that is extracted from the result of certain version
control commands.
<br>&nbsp;</div>

<div CLASS="Body"><a NAME="pgfId-998382"></a>Where replacements are mentioned
in the option descriptions above, legal values are a list of zero or more
pairs. A pair is defined to be a list of exactly two elements. The left-hand
side of the pair is a Tcl regular expression and the right-hand side is
a literal string to replace the text matched by the regular expression.
<br>&nbsp;</div>

<div CLASS="Body"><a NAME="pgfId-998388"></a>An example of a replacement
list is:</div>

<pre CLASS="CodeExample"><a NAME="pgfId-998390"></a>&nbsp; { {"\t" " "} {"foo" "bar"} {"^-+$" ""} }</pre>

<div CLASS="Body"><a NAME="pgfId-998392"></a>When applied to output from
a version control command, this would cause:</div>

<ul>
<li CLASS="Bullet">
<a NAME="pgfId-998395"></a>tabs to be replaced by spaces.</li>

<li CLASS="Bullet">
<a NAME="pgfId-1000121"></a>all occurrences of <tt>foo</tt> to be replaced
by <tt>bar</tt> .</li>

<li CLASS="Bullet">
<a NAME="pgfId-1000124"></a>separation lines consisting of hyphens to be
replaced by a blank line.</li>
</ul>

<h3 CLASS="Heading2">
<a NAME="pgfId-998400"></a>Scripts</h3>

<div CLASS="Body"><a NAME="pgfId-998402"></a>Situations may arise in which
a version control system will seem incompatible with Source-Navigator's
approach to constructing command lines and examining the output of the
command.
<br>&nbsp;</div>

<div CLASS="Body"><a NAME="pgfId-998406"></a>In most cases, these issues
are overcome by writing shell scripts that provide a wrapper interface
that Source-Navigator works with, but internally issues commands that work
with the version control system.
<br>&nbsp;</div>

<div CLASS="Body"><a NAME="pgfId-998411"></a>Consider a version control
system that does not adhere to the convention of specifying the version
number of a file prior to the filename on the command line. Systems such
as RCS would expect a command line such as:</div>

<pre CLASS="CodeExample"><a NAME="pgfId-998416"></a> co -r1.5 main.c</pre>

<div CLASS="Body"><a NAME="pgfId-998418"></a>But other systems might expect
a command line like:</div>

<pre CLASS="CodeExample"><a NAME="pgfId-998420"></a> foocs checkout main.c/1.5</pre>

<div CLASS="Body"><a NAME="pgfId-998422"></a>This could be handled by writing
a small shell script that maps the command lines accordingly:</div>

<pre CLASS="CodeExample"><a NAME="pgfId-998425"></a> #!/bin/sh
&nbsp;foocs checkout $2/$1</pre>

<div CLASS="Body"><a NAME="pgfId-998428"></a>You would then instruct Source-Navigator
to work via the shell script:
<pre CLASS="CodeExample"><a NAME="pgfId-998430"></a> -checkout-individual "foocs-wrapper"</pre>

<div CLASS="Body"><a NAME="pgfId-998432"></a>Shell scripts can also be
useful in situations where a particular operation requires more than one
command to be issued to the version control system.</div>

<h4 CLASS="Heading3">
<a NAME="pgfId-998437"></a><a NAME="14889"></a>Example</h4>

<div CLASS="Body"><a NAME="pgfId-998439"></a>The following example is based
on the GNU RCS version control system, which many developers are familiar
with. Cygnus has integrated Source-Navigator with RCS and this configuration
example is taken directly from the <tt>sn_prop.cfg</tt> configuration file.</div>

<pre CLASS="CodeExample"><a NAME="pgfId-998444"></a># GNU Revision Control System (RCS)
sn_add_version_control_system rcs -default yes \
&nbsp; -checkin "ci" \
&nbsp; -checkin-comment-via stdin \
&nbsp; -checkin-exclusive "ci -l" \
&nbsp; -checkout "co -f" \
&nbsp; -checkout-exclusive "co -f -l" \
&nbsp; -checkout-individual "co -f -r" \
&nbsp; -checkout-individual-to-stdout "co -p" \
&nbsp; -checkout-with-lock yes \
&nbsp; -delete-revision "rcs -o" \
&nbsp; -discard "unco" \
&nbsp; -history "rlog" \
&nbsp; -history-pattern { {"^-----" "^====="} } \
&nbsp; -history-individual "rlog -r" \
&nbsp; -history-individual-pattern { {"^-----" "^====="} } \
&nbsp; -history-replacements { {"[ \t]+" " "} } \
&nbsp; -ignore-dirs RCS \
&nbsp; -lock "rcs -l" \
&nbsp; -lock-individual "rcs -l" \
&nbsp; -revision-number-pattern "^revision (\[0-9.\]+)" \
&nbsp; -symbolic-tags-pattern { {"^symbolic names" "^keyword"} } \
&nbsp; -symbolic-tags-replacements { {"\t" ""} } \
&nbsp; -unlock "rcs -u" \
&nbsp; -unlock-individual "rcs -u"</pre>
</div>

</body>
</html>
